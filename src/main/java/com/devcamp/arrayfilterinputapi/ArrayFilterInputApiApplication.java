package com.devcamp.arrayfilterinputapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArrayFilterInputApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArrayFilterInputApiApplication.class, args);
	}

}
